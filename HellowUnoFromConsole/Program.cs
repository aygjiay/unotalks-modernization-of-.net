﻿const string defaultName = "Unknown Person";

// Template strings support include build time constants
//const string defaultHelloMessage = $"Hello, {defaultName} we are trying .NET 6.0 templates!";

var arg = args.Length > 0 ? args[0] : defaultName;

//var arg = args != default && args.Length > 0 ? args[0] : defaultName;

// Are you serious using Linq extensions without explicit import statement?
//var arg = args != default && args.Count() > 0 ? args[0] : defaultName;

// See https://aka.ms/new-console-template for more information
Console.WriteLine($"Hello, {arg} we are trying .NET 6.0 templates!");
