﻿namespace HelloUnoFromWebApi;

public readonly record struct Framework(string? Name, int Version, CodingLanguage? CodingLanguage);
