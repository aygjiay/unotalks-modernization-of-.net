﻿using Microsoft.AspNetCore.Mvc;

namespace HelloUnoFromWebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class FrameworkController : ControllerBase
{
    [HttpGet]
    public IEnumerable<Framework> RecordStructs()
    {
        var cLanguage = new CodingLanguage { Name = "C#" };
        var typescriptLanguage = cLanguage with { Name = "Typescript" };

        var framework = new Framework
        {
            Name = ".NET",
            Version = 6,
            CodingLanguage = cLanguage
        };

        var angular = framework with { CodingLanguage = typescriptLanguage, Name = "Angular" };

        var frameworks = new List<Framework>
        {
            angular,
            framework
        };

        return frameworks;
    }
}
