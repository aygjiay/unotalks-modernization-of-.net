﻿namespace HelloUnoFromWebApi;

public readonly record struct CodingLanguage(string? Name);